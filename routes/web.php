<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix'        => 'api/v1',
], function () {

    Route::get('/users', 'UserController@index');

    Route::get('/user/{id}', 'UserController@show');

//    Route::get('/user/{id}', function($id){
//        $user = \App\User::find($id);
//
//        if(! $user){
//            return \Illuminate\Support\Facades\Response::json([
//                'error' => 'data not found',
//            ], 404);
//        }
//
//
//        return \Illuminate\Support\Facades\Response::json([
//            'data' => $user,
//        ], 200);
//        return $users;
//    });

});
