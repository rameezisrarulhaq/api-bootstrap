<?php

namespace App\Interfaces;

interface UserRepositoryInterface
{
    /**
     * Get's a $user by it's ID
     *
     * @param int
     */
    public function get($userId);

    /**
     * Get's all $users.
     *
     * @return mixed
     */
    public function all();

}
