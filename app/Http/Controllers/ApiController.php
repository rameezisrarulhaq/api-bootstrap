<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{
    const HTTP_NOT_FOUND = 404;
    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return ApiController
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }


    public function respondNotFound($message = 'Not found!')
    {

        return $this->setStatusCode(self::HTTP_NOT_FOUND)->respondWithError($message);
    }

    public function respondInterError($message = 'Internal Error!')
    {

        return $this->setStatusCode(\Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }

    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'statusCode' => $this->getStatusCode()
            ],
        ]);
    }

    protected function respondCreated($message){

        return $this->setStatusCode(201)->resoond([
            'message'=> $message
        ]);
    }
}
