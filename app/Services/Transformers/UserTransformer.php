<?php

namespace App\Services\Transformers;

class UserTransformer extends Transformer
{

    public function transform($user)
    {

        return [
            'fullname' => $user['name']
        ];

    }
}
