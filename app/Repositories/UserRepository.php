<?php

namespace App\Repositories;

use App\Http\Controllers\ApiController;
use App\Http\Resources\UserCollection;
use App\Services\Transformers\UserTransformer;
use App\User;
use App\Interfaces\ChannelRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\User as UserResource;

class UserRepository extends ApiController implements UserRepositoryInterface
{

    protected $userTransformer;

    public function __construct(UserTransformer $userTransformer)
    {

        $this->userTransformer = $userTransformer;

    }

    /**
     * Get's a user by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($userId)
    {
        $user = User::find($userId);

        if (!$user) {

            return $this->respondNotFound('User not found');

        }

        $user = new UserResource($user);

        return $user;

//        return $this->respond([
//            'data' => $this->userTransformer->transform($user)
//        ]);
    }

    /**
     * Get's all users.
     *
     * @return mixed
     */
    public function all()
    {

        $users = new UserCollection(User::paginate());

        return $users;

//        return $this->respond([
//            'data' => $this->userTransformer->transformCollection($users->toArray())
//        ]);
    }

}
